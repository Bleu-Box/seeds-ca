/**
 * An implementation of the seeds CA. The rulestring for this is
 * B2/S, meaning 2 adjactent live cells -> birth, anything else -> death.
 * 
 * This is best viewed with cells being 1 pixel wide; otherwise you just see lots of 'fizzing'.
 * It gets boring after about 3 seconds because all the cool gliders and stuff disappear.
 */

#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const int SCREEN_WIDTH = 250;
const int SCREEN_HEIGHT = 250;
const int CELL_WIDTH = 1;
const int CELL_HEIGHT = 1;
const int COLS = 250;
const int ROWS = 250;

typedef enum {
  CELLTYPE_DEAD,
  CELLTYPE_LIVE,
} CellType;

void renderCell(CellType ctype, int x, int y, SDL_Renderer* ren);
CellType doRule(CellType cells[COLS][ROWS], int x, int y);
CellType getCell(CellType cells[COLS][ROWS], int x, int y);
void setCell(CellType cells[COLS][ROWS], int x, int y, CellType ctype);
int nbs(CellType cells[COLS][ROWS], CellType type, int x, int y);

int main() {
  SDL_Window* win = NULL;
  SDL_Renderer* ren;
  SDL_Event event;
  CellType cells[COLS][ROWS];

  ///// initialize SDL /////
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("SDL had an error: %s\n", SDL_GetError());
    return 0;
  }

  SDL_CreateWindowAndRenderer(SCREEN_WIDTH, SCREEN_HEIGHT, 0, &win, &ren);

  ///// initialize cells /////
  srand(time(NULL)); // seed the RNG using current time

  // fill the cells with random cells

  for (int x = 0; x < COLS; x++) {
    for(int y = 0; y < ROWS; y++) {
      int choice = rand()%150;
      if (choice == 0) cells[x][y] = CELLTYPE_LIVE;
      else cells[x][y] = CELLTYPE_DEAD;
    }
  } 

  ///// main loop /////
  while (1) {
    // exit main loop when window is closed
    if(SDL_PollEvent(&event) && event.type == SDL_QUIT) break;
    
    // draw and update all of the cells
    CellType newCells[COLS][ROWS];
    for (int x = 0; x < COLS; x++) {
      for(int y = 0; y < ROWS; y++) {
	CellType cState = cells[x][y];
	CellType newState = doRule(cells, x, y);

	// update
	newCells[x][y] = newState;
	// render
	renderCell(cState, x, y, ren);
      }
    } 
    
    SDL_RenderPresent(ren);

    // update `cells`    
    memcpy(cells, newCells, sizeof(cells));
  }
    
  ///// clean up /////
  SDL_DestroyRenderer(ren);
  SDL_DestroyWindow(win);
  SDL_Quit();
  return 0;
}

void renderCell(CellType ctype, int x, int y, SDL_Renderer* ren) {
  SDL_Rect rect;
  rect.x = x*CELL_WIDTH;
  rect.y = y*CELL_HEIGHT;
  rect.w = CELL_WIDTH;
  rect.h = CELL_HEIGHT;

  // set color based on cell type
  switch (ctype) {
  case CELLTYPE_DEAD:
    SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
    break;
  case CELLTYPE_LIVE:
    SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
    break;
  }

  SDL_RenderFillRect(ren, &rect);
}

CellType doRule(CellType cells[COLS][ROWS], int x, int y) {
  CellType ctype = cells[x][y];
  int live_nbs = nbs(cells, CELLTYPE_LIVE, x, y);

  if (ctype == CELLTYPE_DEAD && live_nbs == 2) return CELLTYPE_LIVE;
  // uncommenting the following gives you the 'Live Free or Die' CA.
  // else if (ctype == CELLTYPE_LIVE && live_nbs == 0) return CELLTYPE_LIVE;
  else return CELLTYPE_DEAD;
}

int nbs(CellType cells[COLS][ROWS], CellType ctype, int x, int y) {
  int top = (y+ROWS-1)%ROWS;
  int bottom = (y+1)%ROWS;
  int left = (x+COLS-1)%COLS;
  int right = (x+1)%COLS;
  int count = 0;
  
  CellType neighbs[8] = {
    cells[left][top],
    cells[x][top],
    cells[right][top],
    cells[left][y],
    cells[right][y],
    cells[right][bottom],
    cells[x][bottom],
    cells[left][bottom],
  };
  
  for (int i = 0; i < 9; i++) {
    if (neighbs[i] == ctype) count++;
  }

  return count;
}
